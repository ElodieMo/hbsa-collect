package com.example.hbsacollect.service;

import com.example.hbsacollect.model.Membre;
import com.example.hbsacollect.repository.MembreRepository;
import org.springframework.stereotype.Service;


import java.util.Optional;
@Service
public class MembreService {
    private MembreRepository membreRepository;

    public MembreService(MembreRepository membreRepository) {
        this.membreRepository= membreRepository;
    }
    public Membre saveMembre(Membre membre) {
        return membreRepository.save(membre);
    }


    public Iterable<Membre> getMembre() {
        return membreRepository.findAll();
    }
    public Optional<Membre> getMembreById(Long id) {
        return membreRepository.findById(id);
    }
    public Membre postMembre(Membre model) {
        return membreRepository.save(model);
    }

    public void deleteMembre(Long id) {
        membreRepository.deleteById(id);
    }
    public Membre save(Membre model) {

        return membreRepository.save(model);
    }

}
