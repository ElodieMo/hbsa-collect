package com.example.hbsacollect.service;

import com.example.hbsacollect.model.Benevole;
import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.repository.BenevoleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BenevoleService {

    private BenevoleRepository benevoleRepository;

    public BenevoleService(BenevoleRepository benevoleRepository) {
        this.benevoleRepository = benevoleRepository;
    }

    public Iterable<Benevole> getBenevole() {
        return benevoleRepository.findAll();
    }
    public Optional <Benevole> getBenevoleById(Long id) {
        return benevoleRepository.findById(id);
    }
    public Benevole postBenevole(Benevole model) {
        return benevoleRepository.save(model);
    }

    public void deleteBenevole(Long id) {
        benevoleRepository.deleteById(id);
    }
    public Benevole save(Benevole model) {

        return benevoleRepository.save(model);
    }
}
