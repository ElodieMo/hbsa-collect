package com.example.hbsacollect.service;

import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.repository.CollectRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CollectService {

    private CollectRepository collectRepository;

    public CollectService(CollectRepository collectRepository) {
        this.collectRepository= collectRepository;
    }


    public Collect saveCollect(Collect collect) {
        return collectRepository.save(collect);
    }


    public Iterable<Collect> getCollect() {
        return collectRepository.findAll();
    }
    public Optional<Collect> getCollectById(Long id) {
        return collectRepository.findById(id);
    }
    public Collect postCollect(Collect model) {
        return collectRepository.save(model);
    }

    public void deleteCollect(Long id) {
        collectRepository.deleteById(id);
    }
    public Collect save(Collect model) {

        return collectRepository.save(model);
    }

}
