package com.example.hbsacollect.repository;

import com.example.hbsacollect.model.Collect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollectRepository  extends JpaRepository <Collect, Long> {
}
