package com.example.hbsacollect.repository;

import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.model.Membre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MembreRepository extends JpaRepository<Membre, Long> {
}
