package com.example.hbsacollect.controller;

import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.model.Membre;
import com.example.hbsacollect.service.MembreService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class MembreController {

    private final MembreService membreService;

    public MembreController(MembreService membreService) {
        this.membreService = membreService;
    }

    @GetMapping("/membres")
    public Iterable<Membre> getMembre() {
        return membreService.getMembre();
    }

    @GetMapping("/membres/{id}")
    public Optional<Membre> getMembreById(@PathVariable Long id) {
        return membreService.getMembreById(id);
    }

    @PostMapping("/membres")
    public Membre postMembre(@RequestBody Membre model) {
        return membreService.postMembre(model);
    }

    @DeleteMapping("/membres/{id}")
    public void deleteMembre( @PathVariable long id){
        membreService.deleteMembre(id);
        System.out.println("Delete done");
    }
    @PutMapping("membres/{id}")
    public Membre putMembre(@PathVariable long id, @RequestBody Membre membre) {
        Optional<Membre> e = membreService.getMembreById(id);

        if (e.isPresent()) {
            Membre currentMembre = e.get();

            String nom = membre.getNom();

            if (nom != null) {
                currentMembre.setName(nom);
            }
            String prenom = membre.getPrenom();
            if (prenom != null) {
                currentMembre.setPrenom(prenom);
            }
            String mail = membre.getMail();
            if (mail != null) {
                currentMembre.setMail(mail);
            }
            return membreService.save(currentMembre);
        } else {
            return null;
        }
    }
}

