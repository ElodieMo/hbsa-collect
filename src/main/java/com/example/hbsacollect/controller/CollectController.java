package com.example.hbsacollect.controller;

import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.service.CollectService;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:5173")
public class CollectController {

    private final CollectService collectService;

    public CollectController(CollectService collectService) {
        this.collectService = collectService;
    }


    @GetMapping("/point-collects")
    public Iterable<Collect> getCollect() {
        return collectService.getCollect();
    }

    @GetMapping("/point-collect/{id}")
    public Optional<Collect> getCollectById(@PathVariable Long id) {
        return collectService.getCollectById(id);
    }

    @PostMapping("/point-collects")
    public Collect postCollect(@RequestBody Collect model) {
        return collectService.postCollect(model);
    }

    @DeleteMapping("/point-collects/{id}")
    public void deleteCollect( @PathVariable long id){
        collectService.deleteCollect(id);
        System.out.println("Delete done");
    }
    @PutMapping("point-collects/{id}")
    public Collect putCollect(@PathVariable long id, @RequestBody Collect collect) {
        Optional<Collect> e = collectService.getCollectById(id);

        if (e.isPresent()) {
            Collect currentCollect = e.get();

            String nom = collect.getNom();

            if (nom != null) {
                currentCollect.setNom(nom);
            }
            String horaires = collect.getHoraires();
            if (horaires != null) {
                currentCollect.setHoraires(horaires);
            }
            String adresse = collect.getAdresse();
            if (adresse != null) {
                currentCollect.setAdresse(adresse);
            }
            String code_postal = collect.getCode_postal();
            if (code_postal != null) {
                currentCollect.setCode_postal(code_postal);
            }
            String ville = collect.getVille();
            if (ville != null) {
                currentCollect.setVille(ville);
            }

            Float lattitude = collect.getLattitude();
            if (lattitude != null) {
                currentCollect.setLattitude(lattitude);
            }
            Float longitude = collect.getLongitude();
            if (longitude != null) {
                currentCollect.setLongitude(longitude);
            }
            return collectService.save(currentCollect);
        } else {
            return null;
        }
    }
}
