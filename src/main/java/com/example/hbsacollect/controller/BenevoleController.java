package com.example.hbsacollect.controller;

import com.example.hbsacollect.model.Benevole;
import com.example.hbsacollect.model.Collect;
import com.example.hbsacollect.service.BenevoleService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BenevoleController {

    private final BenevoleService benevoleService;

    public BenevoleController(BenevoleService benevoleService) {
        this.benevoleService = benevoleService;
    }

    @GetMapping("/benevoles")
    public Iterable<Benevole> getBenevole() {
        return benevoleService.getBenevole();
    }

    @GetMapping("/benevole/{id}")
    public Optional<Benevole> getBenevoleById(@PathVariable Long id) {
        return benevoleService.getBenevoleById(id);
    }


    @PostMapping("/benevole")
    public Benevole postBenevole(@RequestBody Benevole model) {
        return benevoleService.postBenevole(model);
    }

    @DeleteMapping("/benevoles/{id}")
    public void deleteBenevole(@PathVariable long id) {
        benevoleService.deleteBenevole(id);
        System.out.println("Delete done");
    }

    @PutMapping("benevoles/{id}")
    public Benevole putBenevole(@PathVariable long id, @RequestBody Benevole benevole) {
        Optional<Benevole> e = benevoleService.getBenevoleById(id);

        if (e.isPresent()) {
            Benevole currentBenevole = e.get();

            String nom = benevole.getNom();

            if (nom != null) {
                currentBenevole.setNom(nom);
            }
            String prenom = benevole.getPrenom();
            if (prenom != null) {
                currentBenevole.setPrenom(prenom);
            }
            String telephone = benevole.getTelephone();
            if (telephone != null) {
                currentBenevole.setTelephone(telephone);
            }
            String mail = benevole.getMail();
            if (mail != null) {
                currentBenevole.setMail(mail);
            }
            return benevoleService.save(currentBenevole);
        } else {
                return null;
            }
        }
    }