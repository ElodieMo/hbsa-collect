package com.example.hbsacollect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HbsaCollectApplication {

	public static void main(String[] args) {
		SpringApplication.run(HbsaCollectApplication.class, args);
	}

}
