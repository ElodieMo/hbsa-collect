package com.example.hbsacollect.model;



import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "pointCollect")
public class Collect {
    @ManyToMany(mappedBy = "pointCollect")
private List<Membre>  membreList = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column(nullable = false)
    private String nom;

    public void setLattitude(Float lattitude) {
        this.lattitude = lattitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Column(nullable = false)
    private String horaires;
    @Column(nullable = false)
    private String ville;

    @Column(nullable = false)
    private String adresse;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHoraires() {
        return horaires;
    }

    public void setHoraires(String horaires) {
        this.horaires = horaires;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public float getLattitude() {
        return lattitude;
    }

    public void setLattitude(float lattitude) {
        this.lattitude = lattitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    private String code_postal;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Column(name = "coordonnees")
    private Float lattitude;
    private Float longitude;

    public List<Membre> getMembreList() {
        return membreList;
    }

    public void setMembreList(List<Membre> membreList) {
        this.membreList = membreList;
    }
}