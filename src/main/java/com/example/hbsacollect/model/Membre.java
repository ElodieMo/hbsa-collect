package com.example.hbsacollect.model;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Membre")
public class Membre {
    @ManyToMany
    @JoinTable
            (name ="member_collect",
    joinColumns = @JoinColumn(name = "member_id"),
    inverseJoinColumns = @JoinColumn(name = "collect_id")
    )
    private List<Collect> pointCollect = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setName(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false)
    private String prenom;

    @Column(nullable = false)
    private String mail;

    public List<Collect> getPointCollect() {
        return pointCollect;
    }

    public void setPointCollect(List<Collect> pointCollect) {
        this.pointCollect = pointCollect;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
