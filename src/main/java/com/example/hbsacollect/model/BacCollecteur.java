package com.example.hbsacollect.model;

import javax.persistence.*;

import java.util.Date;

@Entity(name = "BacCollecteur")
public class BacCollecteur {
    @OneToOne
   private Collect collect;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean bac_plein ;

    private Date date_collect;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getBac_plein() {
        return bac_plein;
    }

    public void setBac_plein(Boolean bac_plein) {
        this.bac_plein = bac_plein;
    }

    public Date getDate_collect() {
        return date_collect;
    }

    public void setDate_collect(Date date_collect) {
        this.date_collect = date_collect;
    }

    public Date getDate_alert() {
        return date_alert;
    }

    public void setDate_alert(Date date_alert) {
        this.date_alert = date_alert;
    }

    public int getQuantites_sac() {
        return quantites_sac;
    }

    public void setQuantites_sac(int quantites_sac) {
        this.quantites_sac = quantites_sac;
    }

    public String getStatut_recolte() {
        return statut_recolte;
    }

    public void setStatut_recolte(String statut_recolte) {
        this.statut_recolte = statut_recolte;
    }

    private Date date_alert;

    private int quantites_sac;

    private String statut_recolte;

    public Collect getCollect() {
        return collect;
    }

    public void setCollect(Collect collect) {
        this.collect = collect;
    }
}
